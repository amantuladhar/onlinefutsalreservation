## Prequesite:
1. Create a database `sampleproject`
2. Download [Node.js][]:
3. Run `npm install -g bower`
4. Run `npm install -g gulp`

## Run Project (Simple Way)
- Run `npm install`
- Run `bower install`
- Run `gulp install`
- Run `./mvnw spring-boot:run` (For Windows Run `mvnw spring-boot:run`)

## What is [Bower][]
- **Manage JavaScript Dependencies** - You can upgrade dependencies by
specifying a newer version in `bower.json`. You can also run `bower update` and `bower install` to manage dependencies.

## What is [gulp][]?

- **Automation** - `gulp` is a toolkit that helps you automate painful or time-consuming tasks in your development workflow. We use gulp mostly when we want to make our application production ready by using `minificaiton` and `concatination` of different `css` and `JavaScript` files.
- **[BrowserSync][]** - We use gulp to start the browerSync which auto-reloads our application when `JavaScript`, `CSS` or `HTML` files are changed. **NOTE** : If you run your application with `./mvnw spring-boot:run` this feature is not enabled. 

## Development
Run the following commands in two separate terminals to create a blissful development experience where your browser
auto-refreshes when files change on your hard drive.

    ./mvnw
    gulp


## Production

To optimize the Application for production, run:

    ./mvnw -Pprod clean package

This will concatenate and minify CSS and JavaScript files using `gulp`. It will also modify `index.html` so it references
these new files.

To ensure everything worked, run:

    java -jar target/*.war

Then navigate to [http://localhost:8080](http://localhost:8080) in your browser.

## Tips for [Jenkins][]

To setup this project in Jenkins, use the following configuration:

* Project name: `SampleProject`
* Source Code Management
    * Git Repository: `reposiroty-url-here`
    * Branches to build: `*/master`
    * Additional Behaviours: `Wipe out repository & force clone`
* Build Triggers
    * Poll SCM / Schedule: `H/5 * * * *`
* Build
    * Invoke Maven / Tasks: `-Pprod clean package`
* Post-build Actions
    * Publish JUnit test result report / Test Report XMLs: `build/test-results/*.xml`

[Node.js]: https://nodejs.org/
[Jenkins]: https://jenkins.io/
[Bower]: http://bower.io/
[Gulp]: http://gulpjs.com/
[BrowserSync]: http://www.browsersync.io/